#include <Arduino.h>
#include <WiFiS3.h>

#include "WatertankNode.h"
#include "PumpCtl.h"
#include "QueueCtl.h"
#include "RelaisCtl.h"
#include "ShuntCtl.h"
#include "WaterLevelCtl.h"
#include "WaterLevelCtlConstants.h"
#include "WifiCtl.h"

// shunt
#define SHUNT_BAT_ADDR INA219_ADDR_1
// #define SHUNT_SOLAR_ADDR INA219_ADDR_2

// create object for wifi, wificlient and mqttclient
CWifi wifi;
WiFiClient wifiClient;
MqttClient mqttClient(wifiClient);

// create our controller objects for wifi/mqtt/relais/waterlevel
WifiCtl wifiCtl(WIFI_SSID, WIFI_PASS, HOSTNAME, wifi, wifiClient);
QueueCtl queueCtl(mqttClient, BROKER, BOKER_PORT, ALIVETOPIC, ALIVE_INTERVAL,
                  WATERLEVEL_TOPIC, SHUNTBAT_TOPIC, SHUNTSOLAR_TOPIC);
RelaisCtl pumpOneRelais(PUMPONE_RELAIS_PIN, LOW);
RelaisCtl resetRelais(RESET_RELAIS_PIN, LOW);
RelaisCtl spareRelais(SPARE_RELAIS_PIN, LOW);
WaterLevelCtl waterLevel(WL_SENSOR_PIN, WL_SENSOR_MIN, WL_SENSOR_MAX,
                         WL_LITER_MIN, WL_LITER_MAX, WL_MEASURE_INTERVAL,
                         WL_MEASURE_MAX, WL_FLATEN_OFFSET);
PumpCtl pumpOne(pumpOneRelais, PUMPONE_DIR, PUMPONE_LPM);

void resetHandler()
{
  Serial.println("resetHandler - triggered.. bye!!");
  pumpOneRelais.off();
  spareRelais.off();
  queueCtl.disconnect();
  wifiCtl.disconnect();
  Serial.end();

  // and we use the reset relais to restart ourself
  resetRelais.on();
  while (true)
  {
    // we should never land here btw..
    // but this keeps ardiono from looping in reboot case
  }
}

ShuntCtl shuntBat(SHUNT_BAT_ADDR, resetHandler);
// ShuntCtl shuntSolar(SHUNT_SOLAR_ADDR, resetHandler);

void messageHandler(int messageSize)
{
  if (queueCtl._client.messageDup())
  {
    Serial.println("messageHandler - duplicated msg received - skipping");
    return;
  }

  String messageTopic = queueCtl._client.messageTopic();
  String message = "";

  Serial.print("messageHandler - received msg on topic: ");
  Serial.println(messageTopic);

  for (int i = 0; i < messageSize; i++)
  {
    // we read each byte from the message
    char byteRead = (char)queueCtl._client.read();
    // check if the byte is a digit
    if (isdigit(byteRead))
    {
      message += byteRead;
    }
    else
    {
      // if not we return out of the handler
      Serial.println("messageHandler - received jibberish, returning");
      return;
    }
  }

  // we kinda also read every byte in case we get shit
  // cause if we get a value for exmplae like "23b10"
  // we actualy do nothing cause we returned up there
  // so we do later only on correct integer value message

  if (messageTopic == RESET_CTLTOPIC)
  {
    // for now we have no logic like reset in a certain time
    // like we do later with the pumpRuntime
    // but we can do that later, for now reset will be triggered if correct
    // topic and message is an integer value
    resetHandler();
  }
  else if (messageTopic == PUMPONE_CTLTOPIC)
  {
    int liter = message.toInt();
    pumpOne.setRuntimeLiter(liter);
  }
  else
  {
    return;
  }
}

void setup()
{
  // init serial
  Serial.begin(9600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  delay(5000);

  Serial.println(); // new line at the start if the program
  Serial.println(
      "setup is running, give it a little time to clean everything up and "
      "reconnect...");

  // set analog read resolution
  analogReadResolution(READ_RESOLUTION);

  // init relais
  pumpOneRelais.init();
  resetRelais.init();
  spareRelais.init();

  // init wifi and mqtt
  wifiCtl.connect(resetHandler);
  queueCtl.connect(resetHandler);
  queueCtl._client.onMessage(messageHandler);
  queueCtl._client.subscribe(RESET_CTLTOPIC);
  queueCtl._client.subscribe(PUMPONE_CTLTOPIC);

  // init water level measurement
  waterLevel.init();

  // init shunts
  shuntBat.init();
  // shuntSolar.init();
}

void loop()
{
  // delay(1500);

  pumpOne.process();

  // then we do checks and alive
  if (!wifi.status() || !queueCtl.status())
  {
    resetHandler();
  }
  queueCtl.sendAlive();

  // then we do measure and sending results if ready
  waterLevel.measure();

  if (waterLevel.getState() == 3)
  {
    // in this case we can send the result to the queue
    int *results = waterLevel.getResults();
    queueCtl.sendWLMeasures(results);
  }

  // do shunt measurements
  shuntBat.measure();
  // shuntSolar.measure();

  if (shuntBat.getState() == 2)
  {
    float *results = shuntBat.getResults();
    queueCtl.sendBatMeasures(results);
  }
  else if (shuntBat.getState() == 3)
  {
    Serial.println("shuntBat state FAILURE");
    resetHandler();
  }

  // if (shuntSolar.getState() == 2)
  // {
  //   float *results = shuntSolar.getResults();
  //   queueCtl.sendSolarMeasures(results);
  //   // call a new method on queueCtl to send battery values
  // }
  // else if (shuntSolar.getState() == 3)
  // {
  //   Serial.println("shuntSolar state FAILURE");
  //   resetHandler();
  // }

  // do a dirty every day reset -> 1 * 24 * 60 * 60 * 1000
  // for testing do every 20min -> 20 * 60 * 1000
  if (millis() > 60 * 60 * 1000)
  {
    resetHandler();
  }
}
